Source: editobj3
Section: python
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Sebastian Ramacher <sramacher@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-setuptools
Standards-Version: 4.7.0
Homepage: http://www.lesfleursdunormal.fr/static/informatique/editobj/index_en.html
Vcs-Git: https://salsa.debian.org/multimedia-team/editobj3.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/editobj3
Rules-Requires-Root: no

Package: python3-editobj3
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Recommends:
# for the Qt UI
 python3-pyqt5,
# for the GTK UI
 python3-gi,
 gir1.2-glib-2.0,
 gir1.2-gtk-3.0,
 git1.2-gdkpixbuf-2.0,
# for the HTML UI
 libjs-jquery,
 python3-aiohttp,
 python3-pil
Description: automatic dialog box generator for Python objects (Python 3)
 Editobj3 is an automatic dialog box generator for Python objects. It supports
 several backends (Qt, GTK, and HTML).
 .
 Editobj3 dialog boxes are composed of an attribute list, a luxurious
 good-looking but useless icon and title bar, and a tree view (if the edited
 object is part of a tree-like structure). Editobj3 includes an advanced
 introspection module that usually guesses how to edit any object; it can also
 be customized for a given class of object through the editobj3.introsp module.
 Editobj3 also supports the simultaneous edition of a group of objects, as if
 they were a single object.
 .
 This package contains the Python 3 module.
